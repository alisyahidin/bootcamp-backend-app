const express = require('express')
const router = express.Router()

const articleCtrl = require('../controllers/article.ctrl.js')

router.get('/', articleCtrl.index)

router.post('/', articleCtrl.store)

router.get('/:id', articleCtrl.show)
router.put('/:id', articleCtrl.update)

router.delete('/:id', articleCtrl.delete)
router.delete('/all', articleCtrl.deleteAll)

module.exports = router