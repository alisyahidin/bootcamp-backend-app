const Article = require('../models/article.js')

module.exports = {
    index: (req, res) => {
        Article.find({}, {'__v': 0}, (err, data) => {
            if (err) throw err
            res.statusCode = 200
            return res.json(data)
        })
    },
    store: (req, res) => {
        Article.create(req.body, (err, article) => {
            if (err) throw err
            res.statusCode = 201
            return res.json(article)
        })
    },
    show: (req, res) => {
        Article.find({'_id': req.params.id}, {'__v': 0}, (err, data) => {
            if (err) throw err
            res.statusCode = 200
            return res.json(data[0])
        })
    },
    update: (req, res) => {
        Article.findById(req.params.id, (err, article) => {
            if (err) throw err

            article.set(req.body);
            article.save(function (err, updatedArticle) {
                if (err) return handleError(err);
                res.statusCode = 200
                res.send(updatedArticle);
            });
        })
    },
    delete: (req, res) => {
        Article.deleteOne({'_id': req.params.id}, (err) => {
            if (err) throw err
            res.send('deleted')
        })
    },
    deleteAll: (req, res) => {
        Article.deleteAll({}, { multi: true }, (err) => {
            if (err) throw err
            res.send('deleted')
        })
    }
}