const express = require('express')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const mongoose = require('mongoose')
const path = require('path')
const cors = require('cors')

mongoose.connect('mongodb://localhost:27017/bootcamp',
    { useNewUrlParser: true }
)

const app = express()

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(methodOverride('_method'))

// Routes Articles
const articleRoutes = require('./routes/article.routes.js')
app.use('/articles', articleRoutes)

app.get('/', (req, res) => res.send('Hello World!'))

app.get('*', (req, res) => res.send('404 Not Found'))

const port = process.env.PORT || 5000

app.listen(port, () => console.log('Server listening at ' + port))